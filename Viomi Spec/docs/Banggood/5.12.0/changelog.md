
---
title: Changelog
---

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][1].

---- 

## [0.1.2] - 2018-05-10
### CHANGED
- 调整选择商品属性的样式

## [0.1.1] - 2018-05-04
### ADDED
- 增加选购产品含有属性选择交互
### REMOVED
- 去掉捆绑销售页面快速选择数量的交互（改由属性页进行选择）

## [0.1.0] - 2018-05-03
### ADDED
- 完成初版交互流程

[1]:	http://keepachangelog.com/en/1.0.0/