
---
title: Changelog
---

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][1].

---- 

## [0.2.4] - 2018-04-13
### CHANGED
- 优化取消选择使用优惠的交互
- 优化已勾选优惠券后编辑使用其他优惠券的交互

## [0.2.3] - 2018-04-09
### CHANGED
- 文案优化
- 优化不可用优惠券展开及收起的交互


## [0.2.2] - 2018-04-08
### ADDED
- 补充没有优惠券的情况
### CHANGED
- 文案调整
- 修改优惠券样式

## [0.2.1] - 2018-04-04
### CHANGED
- 调整编辑 BG Points 的交互

## [0.2.0] - 2018-04-03
### ADDED
- 补充选中 Coupon 时 Place Order 页面的交互
### CHANGED
- 优化 Points 和 Coupon 在 Table View 中的显示
### REMOVED
- 去掉剩余 Points 的显示

## [0.1.0] - 2018-04-03
### ADDED
- 完成初版交互流程

[1]:	http://keepachangelog.com/en/1.0.0/