# 云米 UI

### 项目地址
请访问 [BitBucket][1]。

### 简介
仅供内部同学使用，请勿将相关设计稿外传。
本项目基于 [MKDocs][2] 的 Markdown 项目工具撰写，MKDocs 需要另外安装 [Material][3] 主题。

### 文件结构及规范
- Build 后将 `site` 中的文件全部剪切至项目文件夹中，并删除 `site` 的文件夹

### 图片测试
![][image-1]

[1]:	https://bitbucket.org/viomiui/viomiui.bitbucket.io
[2]:	http://www.mkdocs.org/
[3]:	https://github.com/squidfunk/mkdocs-material

[image-1]:	https://bitbucket.org/viomiui/viomi.ui.image/raw/6f7f6332205462020c4e6098195c08cc960a231c/image/test/Group.png